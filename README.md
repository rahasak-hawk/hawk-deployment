# hawk-deployment

deployment of hawk services


### configur .env

change `host.docker.local` field in `.env` file to local machines ip. also 
its possible to add a host entry to `/etc/hosts` file by overriding
`host.docker.local` with local machines ip. following is an example of
/etc/hosts file

```
10.4.1.104    host.docker.local
```


### misp deployment

deploy misp services in following order

```
docker-compose -f misp/misp-compose.yml up -d mysql
docker-compose -f misp/misp-compose.yml up -d misp
```


### rahasak blockchian deployment

rahasak blockchian can be deployed with single kafka broker or multiple kafka
brokers. there is a separate deployment `kafka-compose.yml` to deploy kafka
cluster with multiple brokers.

```
# with single kafka broker
docker-compose -f misp/rahasak-compose.yml up -d zookeeper
docker-compose -f misp/rahasak-compose.yml up -d kafka
docker-compose -f misp/rahasak-compose.yml up -d redis
docker-compose -f misp/rahasak-compose.yml up -d elassandra
docker-compose -f misp/rahasak-compose.yml up -d aplos
docker-compose -f misp/rahasak-compose.yml up -d lokka
docker-compose -f misp/rahasak-compose.yml up -d kibana // optional service


# with multiple kafka brokers
docker-compose -f misp/kafka-compose.yml up -d
docker-compose -f misp/rahasak-compose.yml up -d redis
docker-compose -f misp/rahasak-compose.yml up -d elassandra
docker-compose -f misp/rahasak-compose.yml up -d aplos
docker-compose -f misp/rahasak-compose.yml up -d lokka
docker-compose -f misp/rahasak-compose.yml up -d kibana // optional service
```
